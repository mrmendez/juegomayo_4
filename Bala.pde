class Bala  {
    float x, y;
    float radio;
    
    Bala(float a, float b, float c){
         x = a;
         y = b;
         radio = c;
         ellipseMode(CENTER);
    }
    
    void dibujaBala(){
       ellipse(x,y,2*radio, 2*radio);
    }
  
    void run(){
       dibujaBala();
    }
  
}
class Circulo{
   float x,y,radio;
   int id;
   
   Circulo(float a, float b, float c, int d){
       x = a;
       y = b;
       radio = c;
       ellipseMode(CENTER);
       id = d;
   }
   
   void dibujaCirculo(){
      fill(255);
      ellipse(x,y,2*radio, 2*radio);
      fill(0);
      text(""+id,x,y);
   }
   
   boolean isColision(float a, float b){
          return dist(a,b,x,y) <= radio;
   }
   
   boolean isColision(Bala a){
         return dist(a.x, a.y, x,y)-radio-a.radio <=0 ;
   }
}